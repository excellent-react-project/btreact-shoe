import React, { Component } from 'react'
import ItemShoe from './ItemShoe'

export default class ListShoe extends Component {
    render() {
        return (
            <div className='row m-3'>
                {this.props.list.map((shoe) => {
                    return <ItemShoe handleOnclick={this.props.handleAddtocart} item={shoe} />
                })}
            </div>
        )
    }
}
